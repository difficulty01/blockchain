/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BlockChain;

/**
 *
 * @author tomas
 */
public class Block {
    String fact;
    int hash;
    int nonce;
    protected int size;
    int previous;
    
    public Block(int previous,String data){
        this.previous=previous;
        this.fact=data;
        this.size=3;
        this.hash=calculateHash();
        //Miner.mine(this); Miner precisa de ser feito
    }
    
    public int calculateHash(){
        return (previous+fact+nonce).hashCode();
    }
    
    public String toString(){
        return String.format("%10d %10s(%d) %10d %b",previous,fact,nonce,hash,isValid());
    }
    
    public boolean isValid(){
        return calculateHash()==hash;
    }
}
