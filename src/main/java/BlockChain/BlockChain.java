/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BlockChain;

import java.util.ArrayList;

/**
 *
 * @author bruno
 */
public class BlockChain {
    ArrayList<Block> chain = new ArrayList<>();
    public void add(String data)
    {
        
        int prev = getLastBlock();
        Block newBlock = new Block(prev,data);
        chain.add(newBlock);
    }
    
    public int getLastBlock()
    {
        if(chain.isEmpty())
        {
            return 0;
        }
        return chain.get(chain.size()-1).hash;
    }
    
    public void print()
    {
        for(Block block:chain)
        {
            System.out.println(block.toString());
        }
    }
}
